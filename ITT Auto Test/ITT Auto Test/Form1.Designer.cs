namespace ITT_Auto_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.toggleStartBtn = new System.Windows.Forms.Button();
            this.functionTestBtn = new System.Windows.Forms.Button();
            this.bluetoothProgBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewPythonFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewComPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutITTAUTOMATIONTESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.showSerialLabel = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.receiveSerialTextbox = new System.Windows.Forms.TextBox();
            this.runningBtn = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.cmdTextBox = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.exitBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toggleStartBtn);
            this.panel1.Controls.Add(this.functionTestBtn);
            this.panel1.Controls.Add(this.bluetoothProgBtn);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(261, 646);
            this.panel1.TabIndex = 0;
            // 
            // toggleStartBtn
            // 
            this.toggleStartBtn.BackColor = System.Drawing.Color.ForestGreen;
            this.toggleStartBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toggleStartBtn.FlatAppearance.BorderSize = 0;
            this.toggleStartBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toggleStartBtn.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toggleStartBtn.ForeColor = System.Drawing.Color.White;
            this.toggleStartBtn.Location = new System.Drawing.Point(0, 537);
            this.toggleStartBtn.Name = "toggleStartBtn";
            this.toggleStartBtn.Size = new System.Drawing.Size(261, 109);
            this.toggleStartBtn.TabIndex = 3;
            this.toggleStartBtn.Text = "CLICK TO START";
            this.toggleStartBtn.UseVisualStyleBackColor = false;
            this.toggleStartBtn.Click += new System.EventHandler(this.toggleStartBtn_Click);
            // 
            // functionTestBtn
            // 
            this.functionTestBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(183)))), ((int)(((byte)(0)))));
            this.functionTestBtn.FlatAppearance.BorderSize = 0;
            this.functionTestBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.functionTestBtn.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.functionTestBtn.ForeColor = System.Drawing.Color.White;
            this.functionTestBtn.Image = global::ITT_Auto_Test.Properties.Resources.computer;
            this.functionTestBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.functionTestBtn.Location = new System.Drawing.Point(0, 335);
            this.functionTestBtn.Name = "functionTestBtn";
            this.functionTestBtn.Size = new System.Drawing.Size(261, 205);
            this.functionTestBtn.TabIndex = 2;
            this.functionTestBtn.Text = "Functional Test";
            this.functionTestBtn.UseVisualStyleBackColor = false;
            this.functionTestBtn.Click += new System.EventHandler(this.functionTestBtn_Click);
            // 
            // bluetoothProgBtn
            // 
            this.bluetoothProgBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(139)))), ((int)(((byte)(168)))));
            this.bluetoothProgBtn.FlatAppearance.BorderSize = 0;
            this.bluetoothProgBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bluetoothProgBtn.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bluetoothProgBtn.ForeColor = System.Drawing.Color.White;
            this.bluetoothProgBtn.Image = global::ITT_Auto_Test.Properties.Resources.bluetooth;
            this.bluetoothProgBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bluetoothProgBtn.Location = new System.Drawing.Point(0, 130);
            this.bluetoothProgBtn.Name = "bluetoothProgBtn";
            this.bluetoothProgBtn.Size = new System.Drawing.Size(261, 205);
            this.bluetoothProgBtn.TabIndex = 1;
            this.bluetoothProgBtn.Text = "Program Bluetooth";
            this.bluetoothProgBtn.UseVisualStyleBackColor = false;
            this.bluetoothProgBtn.Click += new System.EventHandler(this.bluetoothProgBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(212)))), ((int)(((byte)(37)))));
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(261, 646);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(112)))), ((int)(((byte)(129)))));
            this.panel4.Controls.Add(this.menuStrip1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(261, 29);
            this.panel4.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(112)))), ((int)(((byte)(129)))));
            this.menuStrip1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(261, 26);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openConfigurationFileToolStripMenuItem,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(43, 22);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openConfigurationFileToolStripMenuItem
            // 
            this.openConfigurationFileToolStripMenuItem.Name = "openConfigurationFileToolStripMenuItem";
            this.openConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.openConfigurationFileToolStripMenuItem.Text = "Open Configuration file";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(219, 22);
            this.toolStripMenuItem3.Text = "...";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripMenuItem,
            this.viewPythonFileToolStripMenuItem,
            this.viewLimitToolStripMenuItem,
            this.toolStripMenuItem2,
            this.viewComPortToolStripMenuItem});
            this.toolToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(46, 22);
            this.toolToolStripMenuItem.Text = "Tool";
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugToolStripMenuItem,
            this.productionToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.debugToolStripMenuItem.Text = "Debug";
            this.debugToolStripMenuItem.Click += new System.EventHandler(this.debugToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // viewPythonFileToolStripMenuItem
            // 
            this.viewPythonFileToolStripMenuItem.Name = "viewPythonFileToolStripMenuItem";
            this.viewPythonFileToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.viewPythonFileToolStripMenuItem.Text = "View Python file";
            // 
            // viewLimitToolStripMenuItem
            // 
            this.viewLimitToolStripMenuItem.Name = "viewLimitToolStripMenuItem";
            this.viewLimitToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.viewLimitToolStripMenuItem.Text = "View Limit";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(178, 22);
            this.toolStripMenuItem2.Text = "...";
            // 
            // viewComPortToolStripMenuItem
            // 
            this.viewComPortToolStripMenuItem.Name = "viewComPortToolStripMenuItem";
            this.viewComPortToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.viewComPortToolStripMenuItem.Text = "View com port";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutITTAUTOMATIONTESTToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(49, 22);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutITTAUTOMATIONTESTToolStripMenuItem
            // 
            this.aboutITTAUTOMATIONTESTToolStripMenuItem.Name = "aboutITTAUTOMATIONTESTToolStripMenuItem";
            this.aboutITTAUTOMATIONTESTToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.aboutITTAUTOMATIONTESTToolStripMenuItem.Text = "About ITT AUTOMATE TEST";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "ITT AUTOMATE TEST";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.runningBtn);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(261, 537);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(716, 109);
            this.panel2.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 63);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(635, 46);
            this.panel8.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(635, 63);
            this.panel7.TabIndex = 1;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(139)))), ((int)(((byte)(168)))));
            this.panel10.Controls.Add(this.showSerialLabel);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(287, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(348, 63);
            this.panel10.TabIndex = 1;
            // 
            // showSerialLabel
            // 
            this.showSerialLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.showSerialLabel.Font = new System.Drawing.Font("Angsana New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showSerialLabel.ForeColor = System.Drawing.Color.White;
            this.showSerialLabel.Location = new System.Drawing.Point(0, 0);
            this.showSerialLabel.Name = "showSerialLabel";
            this.showSerialLabel.Size = new System.Drawing.Size(348, 63);
            this.showSerialLabel.TabIndex = 0;
            this.showSerialLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(139)))), ((int)(((byte)(168)))));
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.receiveSerialTextbox);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(287, 63);
            this.panel9.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Angsana New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Image = global::ITT_Auto_Test.Properties.Resources.barcode1;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(0, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(287, 37);
            this.label2.TabIndex = 1;
            this.label2.Text = "          SCAN SERIAL NUMBER";
            // 
            // receiveSerialTextbox
            // 
            this.receiveSerialTextbox.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receiveSerialTextbox.Location = new System.Drawing.Point(6, 3);
            this.receiveSerialTextbox.Multiline = true;
            this.receiveSerialTextbox.Name = "receiveSerialTextbox";
            this.receiveSerialTextbox.Size = new System.Drawing.Size(275, 20);
            this.receiveSerialTextbox.TabIndex = 0;
            this.receiveSerialTextbox.WordWrap = false;
            this.receiveSerialTextbox.TextChanged += new System.EventHandler(this.recieveSerialTextbox_TextChanged);
            // 
            // runningBtn
            // 
            this.runningBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.runningBtn.FlatAppearance.BorderSize = 0;
            this.runningBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.runningBtn.ForeColor = System.Drawing.Color.White;
            this.runningBtn.Image = global::ITT_Auto_Test.Properties.Resources.Ellipsis_rz;
            this.runningBtn.Location = new System.Drawing.Point(635, 0);
            this.runningBtn.Name = "runningBtn";
            this.runningBtn.Size = new System.Drawing.Size(81, 109);
            this.runningBtn.TabIndex = 0;
            this.runningBtn.Text = "RUNNING...";
            this.runningBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.runningBtn.UseVisualStyleBackColor = true;
            this.runningBtn.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(85)))), ((int)(((byte)(82)))));
            this.panel5.Controls.Add(this.panelLeft);
            this.panel5.Controls.Add(this.cmdTextBox);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(261, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(716, 537);
            this.panel5.TabIndex = 2;
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(212)))), ((int)(((byte)(37)))));
            this.panelLeft.Location = new System.Drawing.Point(0, 127);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(5, 205);
            this.panelLeft.TabIndex = 3;
            // 
            // cmdTextBox
            // 
            this.cmdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(85)))), ((int)(((byte)(82)))));
            this.cmdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cmdTextBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdTextBox.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdTextBox.ForeColor = System.Drawing.Color.Lime;
            this.cmdTextBox.Location = new System.Drawing.Point(5, 29);
            this.cmdTextBox.MinimumSize = new System.Drawing.Size(200, 200);
            this.cmdTextBox.Multiline = true;
            this.cmdTextBox.Name = "cmdTextBox";
            this.cmdTextBox.Size = new System.Drawing.Size(711, 508);
            this.cmdTextBox.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(112)))), ((int)(((byte)(129)))));
            this.panel6.Controls.Add(this.exitBtn);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(716, 29);
            this.panel6.TabIndex = 2;
            this.panel6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel6_MouseDown);
            this.panel6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel6_MouseMove);
            this.panel6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel6_MouseUp);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(87)))), ((int)(((byte)(59)))));
            this.exitBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.Location = new System.Drawing.Point(680, 0);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(36, 29);
            this.exitBtn.TabIndex = 0;
            this.exitBtn.Text = "X";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(20)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(977, 646);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button functionTestBtn;
        private System.Windows.Forms.Button bluetoothProgBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button toggleStartBtn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutITTAUTOMATIONTESTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewPythonFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem viewComPortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.TextBox cmdTextBox;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox receiveSerialTextbox;
        private System.Windows.Forms.Label showSerialLabel;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button runningBtn;
    }
}

