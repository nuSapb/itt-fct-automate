using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace ITT_Auto_Test
{

    public partial class Form1 : Form
    {

        //************************************************************************************//
        /// Initial global variable
        

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AttachConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        Thread pythonExecThread = null;
        Thread toggleEffectStartThread = null;

        volatile bool runing = false;
        volatile bool isPowerOn = false;
        volatile bool isExec = true;
        volatile bool isProductionMode = false;
        volatile bool isProgramBluetoothPass = false;
        volatile bool isFunctionalTestPass = false;
        volatile bool isSerialNum = false;



        //************************************************************************************//


        public Form1()
        {
            InitializeComponent();
            cmdTextBox.ScrollBars = ScrollBars.Both;
            cmdTextBox.Multiline = true;
            cmdTextBox.WordWrap = false;
            
        }

        

        private void toggleStartBtn_Click(object sender, EventArgs e)
        {
            if (isProductionMode)
            {
                Console.WriteLine("Running in Production Mode");
                panelLeft.Height = toggleStartBtn.Height;
                panelLeft.Top = toggleStartBtn.Top;
                if (isExec == true)
                {
                    pythonExecThread = new Thread(() => pythonExec());
                    toggleEffectStartThread = new Thread(() => toggleEffectStart());
                    //runningImageThread.Start();
                    pythonExecThread.Start();
                    toggleEffectStartThread.Start();
                    receiveSerialTextbox.Invoke(new Action(() => receiveSerialTextbox.Enabled = false));

                    isExec = false;

                }
                else if (isExec == false)
                {
                    receiveSerialTextbox.Invoke(new Action(() => receiveSerialTextbox.Enabled = true));
                    toggleEffectStop();
                    pythonExecThread.Abort();
                    Console.WriteLine("pythonExecThread Aborted");

                    isExec = true;
                }
            }
            else
            {
                MessageBox.Show("Running in DUBUG mode");
            }
        }


        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }


        public void pythonExec()
        {
            Console.WriteLine("pythonExec Started");
            uutPowerOn();
            cmdTextBox.Invoke(new Action(() => cmdTextBox.Clear()));
            string pythonProg = @"C:\ProgramData\Anaconda3\python.exe";
            string targetFile = @"D:\temp.py";
            ProcessStartInfo pythonExec = new ProcessStartInfo(pythonProg, targetFile);
            pythonExec.UseShellExecute = false;
            pythonExec.RedirectStandardOutput = true;
            pythonExec.CreateNoWindow = true;

            var proc = Process.Start(pythonExec);
            string str = proc.StandardOutput.ReadToEnd();
            Console.WriteLine(str);
            cmdTextBox.Invoke(new Action (() => cmdTextBox.Text = str));
            //cmdTextBox.Text = str;
            isExec = true;
            uutPowerOff();
            toggleEffectStop();
            receiveSerialTextbox.Invoke(new Action(() => receiveSerialTextbox.Enabled = true));

        }


        //Toggle button color and change text state when toggleStartBtn clicked
        public void toggleEffectStart()
        {
            runningBtn.Invoke(new Action(() => runningBtn.Visible = true));
            toggleStartBtn.Invoke(new Action(() => toggleStartBtn.Text = "STOP"));
            toggleStartBtn.Invoke(new Action(() => toggleStartBtn.BackColor = System.Drawing.Color.Firebrick));
            
        }

        private void bluetoothProgBtn_Click(object sender, EventArgs e)
        {
            panelLeft.Height = bluetoothProgBtn.Height;
            panelLeft.Top = bluetoothProgBtn.Top;
            if (!isProductionMode)
            {
                MessageBox.Show("DEBUG: Bluetooth Programming");
            }
        }
        private void functionTestBtn_Click(object sender, EventArgs e)
        {
            panelLeft.Height = functionTestBtn.Height;
            panelLeft.Top = functionTestBtn.Top;
            if (!isProductionMode)
            {
                MessageBox.Show("DEBUG: Functional Test");
            }

        }

        private void recieveSerialTextbox_TextChanged(object sender, EventArgs e)
        {
            string serialNo = receiveSerialTextbox.Text;
            showSerialLabel.Text = "Testing :" + serialNo;

        }

        public void toggleEffectStop()
        {
            runningBtn.Invoke(new Action(() => runningBtn.Visible = false));
            //runningBtn.Hide();
            toggleStartBtn.Invoke(new Action(() => toggleStartBtn.Text = "CLICK TO START"));
            //toggleStartBtn.Text = "CLICK TO START";
            toggleStartBtn.Invoke(new Action(() => toggleStartBtn.BackColor = Color.ForestGreen));
            //toggleStartBtn.BackColor = Color.ForestGreen;
        }

        // TODO: Connect to Arduino for Power control [Power On]
        /// <summary>
        /// uutPowerOn:
        /// use for send command to Power On UUT,
        /// that does not return anything and takes no parameters.
        /// </summary>
        private void uutPowerOn()
        {
            Console.WriteLine("UUT Power On");

        }

        // TODO: Connect to Arduino for Power control [Power Off]
        /// <summary>
        /// uutPowerOff:
        /// use for send command to Power Off UUT,
        /// does not return anything and takes no parameters.
        /// </summary>
        private void uutPowerOff()
        {
            Console.WriteLine("UUT Power Off");

        }



        /// <summary>
        /// form, panel, ui, handle event
        /// </summary>
        int mouseX = 0, mouseY = 0;
        bool mouseDown;

        private void panel6_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 500;
                mouseY = MousePosition.Y - 10;

                this.SetDesktopLocation(mouseX, mouseY);
            }

        }

        private void panel6_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }

        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 200;
                mouseY = MousePosition.Y - 10;

                this.SetDesktopLocation(mouseX, mouseY);
            }

        }

        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isProductionMode = true;
            MessageBox.Show("Selected Production mode");
        }

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isProductionMode = false;
            MessageBox.Show("Selected Debug mode");
        }

        private void panel6_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;

        }

        /*
        string standard_output;
        while ((standard_output = myProcess.StandardOutput.ReadLine()) != null) 
        {
            if (standard_output.Contains("xx"))
            {
                //do something
                break;
            }
        }
        */

}
}
