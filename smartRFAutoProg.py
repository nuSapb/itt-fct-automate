#!/bin/python

import subprocess, sys, re

programName = 'SmartRFProgConsole.exe'
firmwareName = 'test.hex'


def run_command(cmdargs):
	p = subprocess.Popen(cmdargs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	output = p.communicate()
	return p.returncode, output[0], output[1]
	
def write_firmware(firmwareName):
	try:
		code, stdout, stderr = run_command([programName, 'S', 'EPV', 'F=%s' % firmwareName])
		if code != 0:
			# log error
			print 'program error(return_code=%d), reason: %s' % (code, stderr)
		else:
			print 'Firmware image burned successfully.'
	except:
		print 'program error:', sys.exc_info(), '\n'
		
		
def receive_mac_addr():
	MAC_ADDR_REGEX = r'IEEE address: ([\w\.]{17})'
	
	try:
		code, stdout, stderr = run_command([programName, 'S', 'RI(F=256, PRI)'])
		if code != 0:
			print 'program error(return_code=%d). reason: %s' % (code, stderr)
		else:
			matches = re.findall(MAC_ADDR_REGEX, stdout)
			if len(matches) == 1:
				mac_addr = matches[0].replace('.', '')
				print 'Receive MAC:', mac_addr
				# save mac address
				return mac_addr
			else:
				print 'None or more MAC address found:', matches
				print stdout
	except:
		print 'program error:', sys.exc_info(), '\n'
	
	
def batch_job():
	print '--- Starting the Firmware Flashing Process ---'
	print '>> Getting MAC address from the device...'
	receive_mac_addr()
	print ' '
	
	print '>> Flashing the firmware to the device...'
	print 'image:', firmwareName
	write_firmware(firmwareName)

if __name__ == '__main__':
	batch_job()
